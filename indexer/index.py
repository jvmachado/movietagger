from indexer import Indexer
from dbconnection import DBConnection
import argparse
import glob
import cv2
 
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required = True)
args = vars(ap.parse_args())
 
cd = Indexer()

client = DBConnection()
db = client.getDB()

for imagePath in glob.glob(args["dataset"] + "/*.jpg"):
	imageID = imagePath[imagePath.rfind("/") + 1:]
 	features = cd.describe(imagePath) 
	features = [float(f) for f in features]
	db.index.insert_one({"imageName": imageID, "features" : features})
 
																																																																																																																																																																																																																																																																																																																																																																																																																																																																							