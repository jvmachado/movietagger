from keras.preprocessing import image
from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_v3 import preprocess_input
from keras.models import Model
import numpy as np
 
class Indexer:
	def __init__(self):
		model = InceptionV3(weights='imagenet')
		model.summary()

		self.bottleneck_model = Model(inputs=model.input, outputs=model.get_layer('avg_pool').output)
		self.bottleneck_model.summary()
		model_json = self.bottleneck_model.to_json()
		self.bottleneck_model.save_weights("model.h5")
		with open("model.json", "w") as json_file:
    			json_file.write(model_json)

		self.imagens = 0
	def describe(self, image_path):
		
		img = image.load_img(image_path, target_size=(224, 224))
		img_data = image.img_to_array(img)
		img_data = np.expand_dims(img_data, axis=0)
		img_data = preprocess_input(img_data)

		inception_feature =  self.bottleneck_model.predict(img_data)
		self.imagens = self.imagens+1
		print('imagens extraídas: '+str(self.imagens))

		return inception_feature.reshape(2048,1).flatten()
