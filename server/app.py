from flask import Flask, render_template,request, json
from flask_cors import CORS
from scipy.misc import imread, imresize
from keras.preprocessing import image
from keras.applications.inception_v3 import preprocess_input
import numpy as np
import keras.models
import re
import base64
import sys 
import os
sys.path.append(os.path.abspath("./model"))
from load import * 
from searcher import Searcher
from moviesearcher import MovieSearcher
app = Flask(__name__)

CORS(app, resources=r'/api/*')
global model
model = init()

def convertImage(imgData1):
	with open('output.png','wb') as output:
		output.write(base64.b64decode(imgData1))
	

@app.route('/api/predict/',methods=['OPTIONS'])
def opt():
	return "ok"


@app.route('/api/predict/',methods=['POST'])
def predict():
	imgData = request.get_json()
	convertImage(imgData['image'])
	x = imread('output.png',mode='L')
	x = np.invert(x)
	x = imresize(x,(28,28))
	x = x.reshape(1,28,28,1)

	img = image.load_img('output.png', target_size=(224, 224))
	img_data = image.img_to_array(img)
	img_data = np.expand_dims(img_data, axis=0)
	img_data = preprocess_input(img_data)
	inception_feature = model.predict(img_data)


	searcher = Searcher()
	movieSearcher = MovieSearcher()
	results = searcher.search(inception_feature.reshape(2048,1).flatten())
	
	for result in results:
		result['movieInfo'] = movieSearcher.search(result['movieName'])

	response = app.response_class(
        response=json.dumps(results),
        status=200,
        mimetype='application/json'
    )
	return response
	

if __name__ == "__main__":
	port = int(os.environ.get('PORT', 8080))
	app.run(host='0.0.0.0', port=port)