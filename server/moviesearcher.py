from dbconnection import DBConnection
from bson.json_util import dumps


class MovieSearcher:
	def search(self, movieName):
		client = DBConnection()
		self.db = client.getDB()
		movie = self.db.movies.find_one({"filme": movieName})
		return {"sinopse":movie['sinopse'],"poster":movie['poster'],"nomeCompletoFilme":movie['nomeCompletoFilme']}