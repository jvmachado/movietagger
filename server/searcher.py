import numpy as np
import csv
from scipy import spatial
from dbconnection import DBConnection
import matplotlib.pyplot as plt
import re

class Searcher:
	def search(self, queryFeatures, limit = 3):
		client = DBConnection()
		self.db = client.getDB()

		results = list()

		index = self.db.index.find()
			
		for doc in index:
			d = spatial.distance.cosine(doc['features'], queryFeatures)
			lastIndex = re.search("\d",doc['imageName'])
			results.append({"imageName":doc['imageName'],"simmilarity":1-d,"movieName": doc['imageName'][:lastIndex.start()+1]})

		results = sorted(results, key=lambda k: k.get('simmilarity', 0), reverse=True)
		return results[:limit]

	def is_numeric(self, number):
		try:
			float(number)
			return True
		except ValueError:
			return False
