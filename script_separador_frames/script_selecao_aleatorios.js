const fs = require('fs');
const diretorio_inicial = process.argv.slice(2)[0]
const diretorio_final = process.argv.slice(2)[1]  

fs.readdir(diretorio_inicial, (err, files) => {
  let numero_arquivos = files.length;
  let arquivos_porcento = parseInt(files.length * 0.1);

  for(let i = 0; i < arquivos_porcento; i++){
      let numero_aleatorio = Math.floor(Math.random()*numero_arquivos);
      let nome_arquivo = files[numero_aleatorio]
      var source = fs.createReadStream(diretorio_inicial + '/' + nome_arquivo);
      var dest = fs.createWriteStream(diretorio_final + '/' + nome_arquivo);
      source.pipe(dest);
      source.on('end', function() { 
        console.log("Arquivo " + nome_arquivo + " copiado") 
        fs.unlink(diretorio_inicial + '/' + nome_arquivo, function(res){ console.log("Arquivo " + nome_arquivo + " removido") })
      });
      source.on('error', function(err) { console.log("Erro ao copiar " + nome_arquivo) });      
      numero_arquivos--;
  }
});